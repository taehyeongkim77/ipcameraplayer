README.md
===

### 개발환경(빌드환경)

* Windows 10 64bits Pro
* Visual Studio 2017(15.0.28307.329)

---

### 라이브러리 다운

* [ffmpeg 다운로드 홈페이지](https://ffmpeg.zeranoe.com/builds/)<br>
* [SDL2 다운로드 홈페이지](https://www.libsdl.org/download-2.0.php)<br>

> (주의) ffmpeg의 경우 dev와 shared를 모두 받아야함 shared에 dll이 있음!
> SDL은 Development Libraries다운!

---

### 잠깐 설명

* FFMPEG : 오픈소스로서 음악과 영상을 녹화하고 변환, 스트리밍 등의 기능을 제공하는 라이브러리
* SDL : (Simple DirectMedia Layer) 여러 그래픽, 사운드, 입력 디바이스에 대한 레이어 인터페이스를 제공하는 크로스 플랫폼

---

### 참고사이트

* [용다TV님 유투브](https://www.youtube.com/watch?v=G61WcnxB4a0)
* [ffmpeg 설명 (네이버N2)](https://d2.naver.com/helloworld/8794)